package top.boyn.coursetable.Secure;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.io.resource.Resource;
import org.junit.Test;
import sun.security.ec.ECPrivateKeyImpl;
import sun.security.ec.ECPublicKeyImpl;

import java.security.*;
import java.security.interfaces.ECKey;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;

/**
 * @author Boyn
 * @date 2019/9/22
 */
public class ECDSATest {
    private static String src = "{'username':2017212036,'password':84328592}";
    @Test
    public void jdkECDSA() throws Exception {
//        Resource resource = new ClassPathResource("classpath:rsa/pri");
//        String privateKey = resource.readUtf8Str();
//        resource = new ClassPathResource("classpath:rsa/pub");
//        String publicKey = resource.readUtf8Str();
//        byte[] pri = Base64.decode(privateKey);
//        byte[] pub = Base64.decode(publicKey);
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC");
        keyPairGenerator.initialize(256);
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        ECPublicKey pubKey = new ECPublicKeyImpl(keyPair.getPublic().getEncoded());
        ECPrivateKey priKey = new ECPrivateKeyImpl(keyPair.getPrivate().getEncoded());
//        PublicKey key = new ECPublicKeyImpl();
//        KeyPair keyPair = new KeyPair()
        PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(priKey.getEncoded());
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        PrivateKey key1 = keyFactory.generatePrivate(pkcs8EncodedKeySpec);
        Signature signature = Signature.getInstance("SHA1withECDSA");
        signature.initSign(key1);
        signature.update(src.getBytes());

        byte[] result = signature.sign();

        System.out.println(Base64.encode(result));
    }
}
