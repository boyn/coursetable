package top.boyn.coursetable.Secure;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileNotFoundException;
import java.security.KeyPair;

/**
 * @author Boyn
 * @date 2019/9/19
 * @description
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class SecureTest {
    @javax.annotation.Resource
    RSA rsa;

    @Test
    public void KeyPairGenerate() throws FileNotFoundException {
        JSONObject object = new JSONObject();
        object.put("username", "2017212036");
        object.put("password", "2017212036");

        byte[] encrypt = rsa.encrypt(StrUtil.bytes(object.toJSONString(), CharsetUtil.CHARSET_UTF_8), KeyType.PublicKey);
        byte[] decrypt = rsa.decrypt(encrypt, KeyType.PrivateKey);
        JSONObject result = JSONObject.parseObject(StrUtil.str(decrypt, CharsetUtil.CHARSET_UTF_8));
        System.out.println(result.toJSONString());
    }

    @Test
    public void PasswordTest(){
        String password = "{\"username\":\"2017212036\",\"password\":\"84328592\"}";
        String encryPassword = rsa.encryptBase64(StrUtil.bytes(password), KeyType.PublicKey);
        log.info(encryPassword);
        encryPassword = "CoG0neOS3yvtkoVJLoBYrAIAD3y6L/ohRw2/ruDMY6CYKXesNfw9xu65SvMinr3dv0XW/N8ZxEBbGE7dhJPU6PeRWclzlPd/+R7PAZU5xmL5lh2odMrGymPPc1+R6tg4mP8zhH+cTAGleLjjfqqoSAd96VYOyeZRlhX0/4fWuRY=";
//        String encryPassword = Base64.encode(encryptPassword);
//        log.info(encryPassword);
//        encryptPassword = Base64.decode(encryPassword);
        log.info(StrUtil.str(rsa.decrypt(Base64.decode(encryPassword), KeyType.PrivateKey), CharsetUtil.CHARSET_UTF_8));
    }


}
