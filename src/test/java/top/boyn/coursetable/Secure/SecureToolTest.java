package top.boyn.coursetable.Secure;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.SecureUtil;
import org.junit.Test;

import java.security.KeyPair;

/**
 * @author Boyn
 * @date 2019/9/22
 */
public class SecureToolTest {
    @Test
    public void generateKeyPaid(){
        KeyPair pair = SecureUtil.generateKeyPair("RSA", 2048);
        byte[] pub = pair.getPublic().getEncoded();
        String pubKey = Base64.encode(pub);
        System.out.println(pubKey);

        byte[] pri = pair.getPrivate().getEncoded();
        String priKey = Base64.encode(pri);
        System.out.println(priKey);
    }
}
