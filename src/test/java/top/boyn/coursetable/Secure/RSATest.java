package top.boyn.coursetable.Secure;

import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.TreeSet;

/**
 * @author Boyn
 * @date 2019/9/21
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RSATest {
    @Resource
    RSA rsa;



    @Test
    public void EncryptTest(){
        JSONObject userInfo = new JSONObject();
        Date now = new Date();
        userInfo.put("request_time", now);
        userInfo.put("user_number","2017212036");
        String string = userInfo.toJSONString();
        rsa.encrypt(string, KeyType.PublicKey);
    }
}
