package top.boyn.coursetable.ControllerTest;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

/**
 * @author Boyn
 * @date 2019/9/10
 * @description
 */
@Ignore
public class SaltController extends BaseControllerTest {
    @Test
    public void getSaltTest() throws Exception{
        String url = "/api/salt";

        MvcResult result = mock
                .perform(MockMvcRequestBuilders.get(url).accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andReturn();
    }
}
