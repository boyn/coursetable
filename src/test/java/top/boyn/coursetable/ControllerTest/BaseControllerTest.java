package top.boyn.coursetable.ControllerTest;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/10
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class BaseControllerTest {
    @Resource
    WebApplicationContext context;

    MockMvc mock;

    @Before
    public void setup(){
        mock = MockMvcBuilders.webAppContextSetup(context).build();
    }
}
