package top.boyn.coursetable.ServiceTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.service.impl.AppGetServiceImpl;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/17
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AppGetServiceTest {

    @Resource
    AppGetServiceImpl service;

    @Test
    public void getSemesterAndWeekTest() throws UserException {
        service.getSemesterAndWeeks(2, Key.USER_KEY);
    }
}
