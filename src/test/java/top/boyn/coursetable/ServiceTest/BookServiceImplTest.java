package top.boyn.coursetable.ServiceTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit4.SpringRunner;
import top.boyn.coursetable.entity.Book;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.repository.BookMongoRepository;
import top.boyn.coursetable.service.impl.BookServiceImpl;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * @author Boyn
 * @date 2019/9/16
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BookServiceImplTest {
    @Resource
    BookServiceImpl service;
    @Resource
    BookMongoRepository repository;

    @Test
    public void getBookInfo() throws UserException {
        /*JSONArray array = service.bookInfo("");
        if (array!=null){
            System.out.println(array.size());
            for (int i = 0; i < array.size(); i++) {
                System.out.println(array.getJSONObject(i).toJSONString());
            }
        }*/

        Book book = service.getBookInfo("高等数学");
        System.out.println(book.getCourseName());
        System.out.println(book.getBookList());

    }

    @Test
    public void saveBookTest() throws UserException {
        service.getBookInfo("模拟电子线路A");
    }

    @Test
    public void searchBookTest(){
        Book book = new Book();
        book.setCourseName("高等数学");
        Optional<Book> result = repository.findOne(Example.of(book));
        result.ifPresent(o -> System.out.println(o.toString()));
    }


}
