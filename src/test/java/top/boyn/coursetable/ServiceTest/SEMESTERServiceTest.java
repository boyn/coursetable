package top.boyn.coursetable.ServiceTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.service.impl.SemesterServiceImpl;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/19
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SEMESTERServiceTest {
    @Resource
    SemesterServiceImpl service;
    @Test
    public void getSemesterWeekFromNetTest() throws UserException {
        service.getSemesterFromNet(Key.USER_KEY);
    }

    @Test
    public void getSemesterWeek(){
        System.out.println(service.getSemesterWeekFromDB());
    }
    @Test
    public void getSemesterWeekByYear(){
        System.out.println(service.getSemesterWeek(2017));

    }
}
