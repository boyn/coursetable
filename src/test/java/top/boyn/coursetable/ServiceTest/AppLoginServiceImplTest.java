package top.boyn.coursetable.ServiceTest;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.service.impl.AppLoginServiceImpl;
import static org.junit.Assert.*;
import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/10
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class AppLoginServiceImplTest {
    @Resource
    AppLoginServiceImpl service;

    @Test
    public void validationTest() throws UserException {
        boolean result = service.validate("2017212036", "84328592");
        assertTrue(result);
        result = service.validate("2017212036", "12345678");
        assertFalse(result);
    }

    @Test
    public void getUserKeyTest(){
//        String userKey1 = service.getUserKey("2017212036", "84328592");
//        String userKey1 = service.getInfo("2017213893", "033519");
//        assertNotNull(userKey1);
//        log.info("userKey:{}",userKey1);
//        String userKey2 = service.getUserKey("2017212036", "84328592");
//        assertNotNull(userKey2);
    }
}
