package top.boyn.coursetable.ServiceTest;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.boyn.coursetable.entity.Course;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.service.impl.CourseTableServiceImpl;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Boyn
 * @date 2019/9/16
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseTableServiceImplTest {

    @Resource
    CourseTableServiceImpl service;

    @Test
    public void getTable() throws UserException {
        /*List<Course> table = service.getCourseTable(String.valueOf(0), String.valueOf(5), "2017212036");
        System.out.println(table);*/
    }

    @Test
    public void getTableFromNet() throws UserException {
        /*List<Course> scheduleByWeek = service.getCourseTableFromNet(0, 3, Key.USER_KEY);
        System.out.println(scheduleByWeek);*/
    }

    @Test
    @Ignore
    public void saveTable() throws UserException {
//        List<Course> scheduleByWeek = service.getCourseTableFromNet(0, 3, Key.USER_KEY);
//        service.saveCourseTable(1L,0,3,scheduleByWeek);
//
//        scheduleByWeek = service.getCourseTableFromMongo(0, 3, 1L);
//        System.out.println(scheduleByWeek);
    }

    @Test
    public void getTableFromMongo(){
        /*List<Course> scheduleByWeek = service.getCourseTableFromMongo(37, 3, 1L);
        System.out.println(scheduleByWeek);*/
    }



   /* @Test
    public void getWeeks(){
        service.getSemesterWeek(Key.USER_KEY);
    }*/
}
