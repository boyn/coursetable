package top.boyn.coursetable.ServiceTest;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.boyn.coursetable.entity.StudentInfo;
import top.boyn.coursetable.service.impl.StudentInfoServiceImpl;
import static org.junit.Assert.*;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/21
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentInfoServiceImplTest {
    @Resource
    StudentInfoServiceImpl service;



    @Test
    @Ignore
    public void insertUser(){
        StudentInfo info = new StudentInfo();
        info.setName("陈展鹏");
        info.setNumber("2017212036");
        info.setPassword("12345678");
        info.setUserKey("9J6i4ywDwv9V21h7J90pZpA5i4DoR6hr63zsmfKrSX8=");
        boolean result = service.insertStudentInfo(info);
        assertTrue(result);

    }

    @Test
    public void getUserKey(){
        String userKey = service.getUserKeyByNumber("1234567");
        assertNull(userKey);
        userKey = service.getUserKeyByNumber("2017212036");
        assertNotNull(userKey);
        assertEquals(userKey,Key.USER_KEY);
    }

    @Test
    public void getStudentInfo(){
        StudentInfo info = service.getStudentInfoByNumber("123456");
        assertNull(info);
        info = service.getStudentInfoByNumber("2017212036");
        System.out.println(info);
        assertNotNull(info);

    }

    @Test
    public void getId() {
        Long id = service.getIdByNumber("2017212036");
        System.out.println(id);
        id = service.getIdByNumber("12456");
        System.out.println(id);
    }

}
