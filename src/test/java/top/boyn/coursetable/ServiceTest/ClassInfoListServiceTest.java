package top.boyn.coursetable.ServiceTest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import top.boyn.coursetable.entity.ClassInfoList;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.service.ClassInfoListService;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/23
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ClassInfoListServiceTest {
    @Resource
    ClassInfoListService service;

    @Test
    public void getClassInfoList() throws UserException {
        ClassInfoList infoList = service.getClassInfoListFromNet("0530310X--001", "035", Key.USER_KEY);
        System.out.println(infoList);
    }

}
