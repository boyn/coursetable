package top.boyn.coursetable;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;

/**
 * @author Boyn
 * @date 2019/9/12
 * @description
 */
public class FastJSONTest {
    @Test
    public void createJSONObjectsAndJSONArray() {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < 2; i++) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("AGE", 10);
            jsonObject.put("FULL NAME", "Doe " + i);
            jsonObject.put("DATE OF BIRTH", "2016/12/12 12:12:12");
            jsonArray.add(jsonObject);
        }
        String jsonOutput = jsonArray.toJSONString();
        System.out.println(jsonOutput);
        JSONArray object = JSON.parseArray(jsonOutput);
        Integer i = object.getJSONObject(0).getInteger("AGE");
        String s = object.getJSONObject(0).getString("FULL NAME");
        JSONObject jo = object.getJSONObject(0).getJSONObject("sth");
        JSONArray ja = object.getJSONObject(0).getJSONArray("sth2");
    }
}
