package top.boyn.coursetable.advice;

import org.springframework.beans.TypeMismatchException;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import top.boyn.coursetable.exception.*;
import top.boyn.coursetable.utils.Result;

import static top.boyn.coursetable.exception.EXCEPTION_CODE.*;

/**
 * @author Boyn
 * @date 2019/9/17
 * @description
 */
@RestControllerAdvice
public class GlobalExceptionHandler{

    @ExceptionHandler(Exception.class)
    public Object HandleException(Exception e) throws UserException {
        throw new UserException(UNKNOWN_ERROR);
    }

    @ExceptionHandler(UserException.class)
    public Object HandleException(UserException e) {
        return Result.UserError().code(e.code).appendInfo(e.message);
    }
}
