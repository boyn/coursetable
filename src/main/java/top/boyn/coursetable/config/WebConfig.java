package top.boyn.coursetable.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.WebRequestInterceptor;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import top.boyn.coursetable.interceptor.JWTInterceptor;

/**
 * @author Boyn
 * @date 2019/9/21
 */
@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

    @Bean
    public WebRequestInterceptor getJWTInterceptor(){
        return new JWTInterceptor();
    }

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addWebRequestInterceptor(getJWTInterceptor()).addPathPatterns("/**").excludePathPatterns("/login/**").excludePathPatterns("/login");
    }

}
