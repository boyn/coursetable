package top.boyn.coursetable.service;

import top.boyn.coursetable.entity.Book;
import top.boyn.coursetable.exception.UserException;

/**
 * @author Boyn
 * @date 2019/9/22
 */
public interface BookService {
    /**
     * 获取对应学科的书本信息
     */
    public Book getBookInfo(String subject) throws UserException;

    /**
     * 查询对应学科的书籍在数据库中存不存在
     */
    public boolean isPresent(Book book);
}
