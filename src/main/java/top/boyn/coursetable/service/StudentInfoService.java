package top.boyn.coursetable.service;

import top.boyn.coursetable.entity.StudentInfo;

/**
 * @author Boyn
 * @date 2019/9/22
 */
public interface StudentInfoService {
    /**
     * 输入学号,获取UserKey
     */
    public String getUserKeyByNumber(String number);

    /**
     * 输入学号,获取学生信息
     */
    public StudentInfo getStudentInfoByNumber(String number);

    public Long getIdByNumber(String number);

    /**
     * 插入一条学生记录,如果在数据库中存在,则更新
     */
    public boolean insertStudentInfo(StudentInfo studentInfo);
}
