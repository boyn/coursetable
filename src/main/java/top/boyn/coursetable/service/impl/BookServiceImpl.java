package top.boyn.coursetable.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import top.boyn.coursetable.entity.Book;
import top.boyn.coursetable.exception.EXCEPTION_CODE;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.repository.BookMongoRepository;
import top.boyn.coursetable.repository.BookNetRepository;
import top.boyn.coursetable.service.BookService;

import javax.annotation.Resource;
import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;


/**
 * @author LJH
 * @date 2019/9/11 21:10
 * <p>
 * BookService返回一个JSON数组
 * 其包含6个元素,每一个元素表示一本书
 * 字段名释义:
 * courseName: 课程名称
 * bookList:
 * - borrowBooks: 可借的书,是一个数组
 * - bookStatus: 表示书是否可借
 * - annVolume: 年卷期
 * - callNum: 索书号
 * - barCodeNum: 条码号
 * - collectPlace: 藏书地点
 * - bookUrl: 书本图片的地址
 * - bookName: 书名
 * - AuthorName: 作者名
 */
@Service
@Slf4j
public class BookServiceImpl implements BookService {
    @Resource
    BookMongoRepository mongoRepository;

    @Resource
    BookNetRepository netRepository;

    /**
     * 获取对应学科的书本信息
     */
    @Override
    public Book getBookInfo(String subject) throws UserException {
        Book book = new Book();
        book.setCourseName(subject);
        Optional<Book> result =
                mongoRepository.findOne(Example.of(book));
        if (result.isPresent()) {
            return result.get();
        } else {
            Book subjectBook = netRepository.getBookInfoFromNet(subject);
            mongoRepository.insert(subjectBook);
            return subjectBook;
        }
    }

    /**
     * 查询对应学科的书籍在数据库中存不存在
     */
    @Override
    public boolean isPresent(Book book) {
        Optional<Book> result =
                mongoRepository.findOne(Example.of(book));
        return result.isPresent();
    }

}
