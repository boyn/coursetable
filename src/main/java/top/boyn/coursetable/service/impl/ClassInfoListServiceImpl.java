package top.boyn.coursetable.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.boyn.coursetable.constant.SEMESTER_MAP;
import top.boyn.coursetable.entity.ClassInfoList;
import top.boyn.coursetable.exception.EXCEPTION_CODE;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.service.ClassInfoListService;
import top.boyn.coursetable.utils.Request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Boyn
 * @date 2019/9/23
 */
@Service
@Slf4j
public class ClassInfoListServiceImpl implements ClassInfoListService {
    private static final String GET_CLASS_LIST_URL = "http://jxglstu.hfut.edu.cn:7070/appservice/home/schedule/getClassList.action";
    private static final String BACHELOR_PROJECT_ID = "2";//本科生ID

    @Override
    public ClassInfoList getClassInfoList(String semesterYear, String semesterPart, String lessonCode, String userKey) throws UserException {
        String semesterCode = SEMESTER_MAP.getSemesterCode(semesterYear, semesterPart);
        return getClassInfoListFromNet(lessonCode, semesterCode, userKey);
    }

    @Override
    public ClassInfoList getClassInfoListFromNet(String lessonCode, String semesterCode, String userKey) throws UserException {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("lessonCode", lessonCode);
        parameters.put("projectId", BACHELOR_PROJECT_ID);
        parameters.put("semestercode", semesterCode);
        parameters.put("userKey", userKey);
        JSONObject netResult = (JSONObject) Request.PostByFormData(GET_CLASS_LIST_URL, parameters);
        JSONArray classList = netResult.getJSONObject("obj").getJSONArray("business_data");
        if (classList.isEmpty()) {
            throw new UserException(EXCEPTION_CODE.COURSE_CODE_ERROR);
        }
        ClassInfoList infoList = new ClassInfoList();
        infoList.setStudents(parseClassListJSON(classList));
        infoList.setLessonCode(lessonCode);
        infoList.setListLength(classList.size());

        return infoList;
    }

    public List<ClassInfoList.StudentInClassInfo> parseClassListJSON(JSONArray classList) {
        List<ClassInfoList.StudentInClassInfo> list = new ArrayList<>();
        for (int i = 0; i < classList.size(); i++) {
            ClassInfoList.StudentInClassInfo info = new ClassInfoList.StudentInClassInfo();
            info.setName(classList.getJSONObject(i).getString("name"));
            info.setClassName(classList.getJSONObject(i).getString("adminclass_name"));
            info.setGender(classList.getJSONObject(i).getString("gender"));
            list.add(info);
        }
        return list;
    }



}
