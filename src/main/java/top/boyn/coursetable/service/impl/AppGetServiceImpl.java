package top.boyn.coursetable.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.boyn.coursetable.exception.EXCEPTION_CODE;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.service.AppGetService;
import top.boyn.coursetable.utils.Request;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Boyn
 * @date 2019/9/11
 * @description 从App中获取信息
 */
@Service
@Slf4j
public class AppGetServiceImpl implements AppGetService {
    //获取学期与周数
    private static final String GET_SEMESTER_AND_WEEKS_URL = "http://jxglstu.hfut.edu.cn:7070/appservice/home/publicdata/getSemesterAndWeekList.action";

    @Override
    public JSONObject getSemesterAndWeeks(int projectId, String userKey) throws UserException {
        if (projectId!=2){
            throw new UserException(EXCEPTION_CODE.ONLY_BACHELOR_ERROR);
        }
        Map<String, String> parameters = new HashMap<>();
        parameters.put("projectId",String.valueOf(projectId));
        parameters.put("userKey",userKey);
        JSONObject object = (JSONObject)Request.PostByFormData(GET_SEMESTER_AND_WEEKS_URL,parameters);
//        log.info(object.toJSONString());
        JSONArray resultArray = new JSONArray();
        JSONArray semesters = object.getJSONObject("obj").getJSONObject("business_data").getJSONArray("semesters");
        for (int i = 0; i < semesters.size(); i++) {
            JSONObject semesterInArray = semesters.getJSONObject(i);
            JSONObject preSemesterJSONObject = new JSONObject();
            preSemesterJSONObject.put("semesterCode",semesterInArray.getString("code"));
            preSemesterJSONObject.put("semesterName",semesterInArray.getString("name"));
            preSemesterJSONObject.put("totalWeeksSize", String.valueOf(semesterInArray.getJSONArray("weeks").size()));
            preSemesterJSONObject.put("startDate", semesterInArray.getJSONArray("weeks").getJSONObject(0).getString("begin_on"));
            resultArray.add(preSemesterJSONObject);
        }
        JSONObject result = new JSONObject();
        result.put("semestersSize", semesters.size());
        result.put("semesters", resultArray);
        log.info(result.toJSONString());
        return result;
    }
}
