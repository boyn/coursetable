package top.boyn.coursetable.service.impl;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.boyn.coursetable.entity.StudentInfo;
import top.boyn.coursetable.repository.StudentInfoRepository;
import top.boyn.coursetable.service.StudentInfoService;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/19
 * @description
 */
@Service
@Slf4j
public class StudentInfoServiceImpl implements StudentInfoService {
    @Resource
    StudentInfoRepository repository;
    @Resource
    RSA rsa;

    /**
     * 输入学号,获取UserKey
     */
    public String getUserKeyByNumber(String number) {
        QueryWrapper<StudentInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("number", number);
        StudentInfo info = repository.selectOne(wrapper);
        return info == null ? null : info.getUserKey();
    }

    /**
     * 输入学号,获取学生信息
     */
    public StudentInfo getStudentInfoByNumber(String number) {
        QueryWrapper<StudentInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("number", number);
        StudentInfo info = repository.selectOne(wrapper);

        if (info == null) {
            return null;
        }
        //从数据库取出数据时需要用私钥进行解密
        info.setPassword(StrUtil.str(Base64.decode(rsa.decrypt(info.getPassword(), KeyType.PrivateKey)), CharsetUtil.CHARSET_UTF_8));
        return info;
    }

    public Long getIdByNumber(String number) {
        QueryWrapper<StudentInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("number", number);
        StudentInfo info = repository.selectOne(wrapper);

        return info == null ? null : info.getID();
    }

    /**
     * 插入一条学生记录,如果在数据库中存在,则更新
     */
    public boolean insertStudentInfo(StudentInfo studentInfo) {
        String password = studentInfo.getPassword();
        //密码在数据库中需要加密存储,用公钥进行加密
        studentInfo.setPassword(rsa.encryptBase64(StrUtil.bytes(password, CharsetUtil.CHARSET_UTF_8), KeyType.PublicKey));
        QueryWrapper<StudentInfo> wrapper = new QueryWrapper<>();
        wrapper.eq("number", studentInfo.getNumber());
        if (repository.selectOne(wrapper) == null) {
            studentInfo.setID(0L);
            int number = repository.insert(studentInfo);
            return number == 1;
        } else {
            int number = repository.update(studentInfo, wrapper);
            return number == 1;
        }
    }
}
