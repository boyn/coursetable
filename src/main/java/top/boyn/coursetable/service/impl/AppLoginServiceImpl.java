package top.boyn.coursetable.service.impl;

import cn.hutool.core.codec.Base64;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.boyn.coursetable.entity.StudentInfo;
import top.boyn.coursetable.exception.EXCEPTION_CODE;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.service.AppLoginService;
import top.boyn.coursetable.utils.Request;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Boyn
 * @date 2019/9/10
 * @description 课表安卓APP端登录服务, 主要用于验证用户名与密码是否正确和获取userKey
 * <p>
 * 请求JSON格式实例:
 * {"msg":"查询成功！","code":200,"obj":
 *      {"err_msg":"",
 *      "err_code":"00000",
 *      "userKey":"9J6i4ywDwv9V21h7J90pZpA5i4DoR6hr63zsmfKrSX8=",
 *      "business_data":
 *          {"adminclass_name":"电**1*-1班",
 *          "birthday":"2000-1-1",
 *          "account_email":"10000000@qq.com",
 *          "gender":"男","user_code":"201*000000",
 *          "mobile_phone":"1*6*2*7*0*0","
 *          user_name":"陈**",
 *          "direction_name":"",
 *          "ancestral_addr":"广*市*珠*石*四*1*号*0*房",
 *          "major_name":"电子信息工程",
 *          "depart_name":"计算机与信息学院"}},
 * "token":"%7***%7D0DCCF***C4E79588C815635*****0627***F4597***5CF26***F6AC*******"
 */
@Slf4j
@Service
public class AppLoginServiceImpl implements AppLoginService {
    private static final String LOGIN_URL = "http://jxglstu.hfut.edu.cn:7070/appservice/home/appLogin/login.action";

    /**
     * 验证用户名与密码是否正确
     *
     * @param number   学号
     * @param password 密码
     * @return
     */
    @Override
    public boolean validate(String number, String password) throws UserException {
        /*try {
            password = new String(Base64.getEncoder().encode(password.getBytes()));
            FormBody body = new FormBody.Builder()
                    .add("username", number)
                    .add("password", password)
                    .add("identity", "0")
                    .build();
            Request request = new Request.Builder()
                    .url(LOGIN_URL)
                    .post(body)
                    .build();
            try (Response response = client.newCall(request).execute()) {
                JSONObject jsonObject = JSON.parseObject((Objects.requireNonNull(response.body())).string());
                Integer code = jsonObject.getInteger("code");
                log.info(jsonObject.toJSONString());

                return code == 200;
            }
        } catch (Exception e) {
            log.error("APP登录URL异常:" + e.getMessage());
            return false;
        }*/
        password = Base64.encode(password);
        Map<String, String> parameters = new HashMap<>();
        parameters.put("username", number);
        parameters.put("password", password);
        parameters.put("identity", "0");
        JSONObject jsonObject = (JSONObject) Request.PostByFormData(LOGIN_URL, parameters);
        Integer code = jsonObject.getInteger("code");
        log.info(jsonObject.toJSONString());
        return code == 200;
    }

    /**
     * @param number
     * @param password
     * @return
     */
    @Override
    public StudentInfo getInfo(String number, String password) throws UserException {
        /*log.info("{} do getUserKey()",number);
        try {
            password = new String(Base64.getEncoder().encode(password.getBytes()));
            FormBody body = new FormBody.Builder()
                    .add("username", number)
                    .add("password", password)
                    .add("identity", "0")
                    .build();
            Request request = new Request.Builder()
                    .url(LOGIN_URL)
                    .post(body)
                    .build();
            try (Response response = client.newCall(request).execute()) {
                JSONObject jsonObject = JSON.parseObject((response.body()).string());
                Integer code = jsonObject.getInteger("code");
                if (code == 200) {
                    String userKey =  jsonObject.getJSONObject("obj").getString("userKey");
                    log.info("{} get the userKey : {}",number,userKey);
                    return userKey;
                }else{
                    log.info("{} wrong password or number",number);
                    return null;
                }
            }
        } catch (Exception e) {
            log.error("APP登录URL异常:" + e.getMessage());
            return null;
        }*/
        password = Base64.encode(password);
        Map<String, String> parameters = new HashMap<>();
        parameters.put("username", number);
        parameters.put("password", password);
        parameters.put("identity", "0");
        JSONObject jsonObject = (JSONObject) Request.PostByFormData(LOGIN_URL, parameters);
        Integer code = jsonObject.getInteger("code");
        if (code == 200) {
            String userKey = jsonObject.getJSONObject("obj").getString("userKey");
            log.info("{} get the userKey : {}", number, userKey);
            StudentInfo info = new StudentInfo();
            info.setUserKey(userKey);
            info.setPassword(password);
            info.setName(jsonObject.getJSONObject("obj").getJSONObject("business_data").getString("user_name"));
            info.setNumber(number);
            return info;
        } else {
            log.info("{} wrong password or number", number);
            throw new UserException(EXCEPTION_CODE.USERNAME_OR_PASSWORD_ERROR);
        }
    }
}
