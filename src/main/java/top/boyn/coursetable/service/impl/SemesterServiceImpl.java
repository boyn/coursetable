package top.boyn.coursetable.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Service;
import top.boyn.coursetable.entity.Semester;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.repository.SemesterRepository;
import top.boyn.coursetable.service.SemesterService;
import top.boyn.coursetable.utils.Request;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Boyn
 * @date 2019/9/19
 * @description
 */
@Service
@Slf4j
public class SemesterServiceImpl implements Ordered, SemesterService {
    @Resource
    SemesterRepository repository;

    private static final String GET_WEEK_URL = "http://jxglstu.hfut.edu.cn:7070/appservice/home/publicdata/getSemesterAndWeekList.action";
    /**
     * 从网络中获取学期与周数的信息,在项目启动时进行检查
     */
    public List<Semester> getSemesterFromNet(String userKey) throws UserException {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("projectId", "2");
        parameters.put("userKey", userKey);
        List<Semester> semesterList = new ArrayList<>();
        JSONObject object = (JSONObject) Request.PostByFormData(GET_WEEK_URL, parameters);
        JSONArray semesterJSONArray = object.getJSONObject("obj").getJSONObject("business_data").getJSONArray("semesters");
        for (int i = 0; i < semesterJSONArray.size(); i++) {
            JSONObject semesterJSONObject = semesterJSONArray.getJSONObject(i);
            Semester semester = new Semester();
            String semesterName = semesterJSONObject.getString("name");
            semester.setSemesterYear(Integer.parseInt(semesterName.substring(0, 4)));
            String part = semesterName.substring(12,13);
            if ("一".equals(part)) {
                semester.setSemesterPart(1);
            } else {
                semester.setSemesterPart(2);
            }
            semester.setCode(semesterJSONObject.getString("code"));
            semester.setName(semesterJSONObject.getString("name"));
            semester.setWeeks(semesterJSONObject.getJSONArray("weeks").size());
            semester.setStartDate(semesterJSONObject.getJSONArray("weeks").getJSONObject(0).getString("begin_on"));
            semesterList.add(semester);
            insertIfNotExists(semester);
            log.info(semester.toString());
        }
        return semesterList;
    }

    /**
     * 从数据库中获取学期与周数
     */
    public List<Semester> getSemesterWeekFromDB(){
        QueryWrapper<Semester> wrapper = new QueryWrapper<>();
        return repository.selectList(wrapper);
    }

    @Override
    public List<Semester> getSemesterWeek(int year) {
        QueryWrapper<Semester> wrapper = new QueryWrapper<>();
        wrapper.ge("semester_year",year);
        wrapper.le("semester_year",year+4);
        return repository.selectList(wrapper);
    }

    /**
     * 如果该条数据不存在,则插入数据库
     */
    private void insertIfNotExists(Semester semester) {
        QueryWrapper<Semester> wrapper = new QueryWrapper<>();
        wrapper.eq("code", semester.getCode());
        if (repository.selectOne(wrapper) == null) {
            log.info(semester.getCode() + " not exist");
            repository.insert(semester);
        } else {
            log.info(semester.getCode()+" exist");
        }
    }

    /**
     * 设置启动顺序为42,要在Application中loadBlackSemester之前进行初始化
     */
    @Override
    public int getOrder() {
        return 42;
    }
}
