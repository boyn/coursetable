package top.boyn.coursetable.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import top.boyn.coursetable.constant.SEMESTER_MAP;
import top.boyn.coursetable.entity.Course;
import top.boyn.coursetable.entity.Semester;
import top.boyn.coursetable.entity.StudentCourse;
import top.boyn.coursetable.entity.StudentInfo;
import top.boyn.coursetable.exception.EXCEPTION_CODE;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.repository.CourseTableRepository;
import top.boyn.coursetable.service.CourseTableService;
import top.boyn.coursetable.utils.Request;
import top.boyn.coursetable.utils.Validator;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

import static top.boyn.coursetable.exception.EXCEPTION_CODE.COURSE_TABLE_NOT_FOUND_ERROR;

/**
 * @author Boyn
 * @date 2019/9/16
 * @description
 */
@Service
@Slf4j
public class CourseTableServiceImpl implements CourseTableService {
    //根据周数获取当周课程表
    private static final String GET_COURSE_TABLE_BY_WEEK_URL = "http://jxglstu.hfut.edu.cn:7070/appservice/home/schedule/getWeekSchedule.action";


    @Resource
    StudentInfoServiceImpl service;

    @Resource
    CourseTableRepository repository;

    /**
     * 外界调用的方法,输入参数,获取对应学期周数的课程表
     *
     * @param semesterYear 学期
     * @param week     周数
     * @param number   学号
     */
    @Override
    public List<Course> getCourseTable(String semesterYear, String part, String week, String number) throws UserException {
        StudentInfo info = service.getStudentInfoByNumber(number);
        if (info == null) {
            throw new UserException(EXCEPTION_CODE.NO_THIS_STUDENT_ERROR);
        }
        Validator.checkSemesterAndWeekIsValid(semesterYear,part, week);
        int yearCode = Integer.parseInt(semesterYear);
        int partCode = Integer.parseInt(part);
        int weekCode = Integer.parseInt(week);
        List<Course> courseTableList = getCourseTableFromMongo(yearCode,partCode, weekCode, info.getID());
        if (courseTableList == null) {
            courseTableList = getCourseTableFromNet(yearCode,partCode,weekCode, info.getUserKey());
            saveCourseTable(info.getID(), yearCode,partCode, weekCode, courseTableList);
            return courseTableList;
        } else {
            return courseTableList;
        }
    }


    /**
     * 将课程表的信息存入到Mongodb中
     *
     * @param studentId       学生信息在mysql - student_info表中对应的ID
     * @param semesterCode    学期代码
     * @param week            周数
     * @param courseTableList 课程表的列表
     */
    private void saveCourseTable(Long studentId, int semesterCode,int part, int week, List<Course> courseTableList) {
        StudentCourse course = new StudentCourse();
        course.setCourseList(courseTableList);
        course.setSemesterYear(semesterCode);
        course.setSemesterPart(part);
        course.setSemesterYear(semesterCode);
        course.setWeek(week);
        course.setStudentID(studentId);
        repository.insert(course);
    }

    /**
     * 从Mongodb中获取课程表
     *
     * @param yearCode 学年
     * @param part 学期
     * @param week 周数
     * @param studentID 学生ID
     * @return 返回对应的课程列表
     */
    private List<Course> getCourseTableFromMongo(int yearCode,int part , int week, Long studentID) {
        log.info("Get from Mongo -- semester: {},part: {}, week: {}", yearCode, part, week);
        StudentCourse course = new StudentCourse();
        course.setSemesterYear(yearCode);
        course.setSemesterPart(part);
        course.setWeek(week);
        course.setStudentID(studentID);
        Optional<StudentCourse> result =
                repository.findOne(Example.of(course));
        return result.map(StudentCourse::getCourseList).orElse(null);
    }


    /**
     * 从APP接口中获取课程表列表并解析
     *
     * @param yearCode 学期代码
     * @param week         周数
     * @param userKey      对应的userKey,存在mysql - student_info表中
     * @return 返回对应的课程列表
     */
    private List<Course> getCourseTableFromNet(int yearCode,int partCode, int week, String userKey) throws UserException {
        String semester = SEMESTER_MAP.getSemesterCode(yearCode, partCode);
        log.info("Get from Net -- semester: {}, part: {}, week: {}, code: {}", yearCode, partCode, week, semester);

        Map<String, String> parameters = new HashMap<>();
        parameters.put("projectId", "2");
        parameters.put("userKey", userKey);
        parameters.put("semestercode", semester);
        parameters.put("weekIndx", String.valueOf(week));
        parameters.put("identity", "0");
        JSONObject courseTable = (JSONObject) Request.PostByFormData(GET_COURSE_TABLE_BY_WEEK_URL, parameters);
        return parseCourseTableJSON(courseTable);
    }


    /**
     * 按JSON文件来解析,生成course对象列表
     */
    private List<Course> parseCourseTableJSON(JSONObject courseTable) throws UserException {
        List<Course> courseList = new ArrayList<>();
        JSONArray table = new JSONArray();
        try {
            table = courseTable.getJSONObject("obj").getJSONArray("business_data");
        } catch (NullPointerException e) {
            throw new UserException(COURSE_TABLE_NOT_FOUND_ERROR);
        }
        for (int i = 0; i < table.size(); i++) {
            JSONObject courseJSONObject = table.getJSONObject(i);
            Course course = new Course();
            course.setCourseName(courseJSONObject.getString("course_name"));
            course.setActivateWeek(courseJSONObject.getString("activity_week"));
            //有时候会有一些没有指定教室的
            if (courseJSONObject.getJSONArray("rooms").size() == 0) {
                course.setCampus("未知");
                course.setClassRoomName("未知");
            } else {
                course.setCampus(courseJSONObject.getJSONArray("rooms").getJSONObject(0).getString("campus_name"));
                course.setClassRoomName(courseJSONObject.getJSONArray("rooms").getJSONObject(0).getString("name"));
            }
            course.setNumber(courseJSONObject.getIntValue("teachclass_stdcount"));
            course.setLesson_code(courseJSONObject.getString("lesson_code"));
            course.setStartUnit(courseJSONObject.getIntValue("start_unit"));
            course.setEndUnit(courseJSONObject.getIntValue("end_unit"));
            JSONArray teachers = courseJSONObject.getJSONArray("teachers");
            String[] teacher = new String[teachers.size()];
            for (int j = 0; j < teachers.size(); j++) {
                teacher[j] = teachers.getJSONObject(j).getString("name");
            }
            course.setTeachers(teacher);
            course.setWeekDay(courseJSONObject.getIntValue("weekday"));
            courseList.add(course);
        }
        return courseList;
    }
}
