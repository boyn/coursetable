package top.boyn.coursetable.service.impl;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import top.boyn.coursetable.entity.StudentInfo;
import top.boyn.coursetable.exception.EXCEPTION_CODE;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.service.AppLoginService;
import top.boyn.coursetable.service.LoginService;
import top.boyn.coursetable.service.StudentInfoService;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/21
 */
@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

    @Resource
    AppLoginService appLoginService;
    @Resource
    StudentInfoService infoService;
    @Resource
    RSA rsa;

    /**
     * 登录方法,首先通过用户名与密码获取UserKey
     * 如果没有出错,则将用户存入数据库中,并签发token返回给上层方法
     */
    @Override
    public String login(String usernameAndPassword) throws UserException {
        String username = usernameAndPassword.split("&")[0];
        String password = usernameAndPassword.split("&")[1];
        StudentInfo info = appLoginService.getInfo(username, password);
        infoService.insertStudentInfo(info);
        JSONObject object = new JSONObject();
        object.put("number", info.getNumber());
        log.info("Login success. User Info: " + object.toJSONString());
        return signToken(object.toJSONString());
    }

    private String signToken(String objectStr) {
        return rsa.encryptBase64(StrUtil.bytes(objectStr, CharsetUtil.CHARSET_UTF_8), KeyType.PublicKey);
    }

    @Override
    public String parseKey(String key) throws UserException {
        key = key.replaceAll(" ", "+");
        String json;
        try {
            json = StrUtil.str(rsa.decrypt(Base64.decode(key), KeyType.PrivateKey), CharsetUtil.CHARSET_UTF_8);
        } catch (Exception e) {
            throw new UserException(EXCEPTION_CODE.KEY_PARSE_ERROR);
        }
        JSONObject jsonObject = JSON.parseObject(json);
        String username = jsonObject.getString("username");
        String password = jsonObject.getString("password");
        if (StrUtil.isBlank(username) || StrUtil.isBlank(password)) {
            throw new UserException(EXCEPTION_CODE.USERNAME_OR_PASSWORD_ERROR);
        }
        return username + "&" + password;
    }
}
