package top.boyn.coursetable.service;

import top.boyn.coursetable.entity.StudentInfo;
import top.boyn.coursetable.exception.UserException;

/**
 * @author Boyn
 * @date 2019/9/22
 */
public interface AppLoginService {
    /**
     * 验证用户名与密码是否正确
     */
    public boolean validate(String number, String password) throws UserException;

    /**
     * 获取info类对象
     */
    public StudentInfo getInfo(String number, String password) throws UserException;
}
