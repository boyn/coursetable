package top.boyn.coursetable.service;

import top.boyn.coursetable.entity.Course;
import top.boyn.coursetable.exception.UserException;

import java.util.List;

/**
 * @author Boyn
 * @date 2019/9/22
 */
public interface CourseTableService {
    /**
     * 外界调用的方法,输入参数,获取对应学期周数的课程表
     *
     * @param semesterCode 学期
     * @param week         周数
     * @param number       学号
     */
    List<Course> getCourseTable(String semesterCode, String part, String week, String number) throws UserException;
}
