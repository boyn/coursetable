package top.boyn.coursetable.service;

import top.boyn.coursetable.exception.UserException;

/**
 * @author Boyn
 * @date 2019/9/22
 */
public interface LoginService {
    /**
     * 登录方法,首先通过用户名与密码获取UserKey
     * 如果没有出错,则将用户存入数据库中,并签发token返回给上层方法
     */
    public String login(String usernameAndPassword) throws UserException;

    public String parseKey(String key) throws UserException;
}
