package top.boyn.coursetable.service;

import com.alibaba.fastjson.JSONObject;
import top.boyn.coursetable.exception.UserException;

/**
 * @author Boyn
 * @date 2019/9/22
 */
public interface AppGetService {
    /**
     * 获取学期与周数
     */
    JSONObject getSemesterAndWeeks(int projectId, String userKey) throws UserException;
}
