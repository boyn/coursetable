package top.boyn.coursetable.service;

import top.boyn.coursetable.entity.ClassInfoList;
import top.boyn.coursetable.exception.UserException;

/**
 * @author Boyn
 * @date 2019/9/23
 */
public interface ClassInfoListService {
    ClassInfoList getClassInfoList(String semesterYear, String semesterPart, String lessonCode, String userKey) throws UserException;

    ClassInfoList getClassInfoListFromNet(String lessonCode, String semesterCode, String userKey) throws UserException;

}
