package top.boyn.coursetable.service;

import top.boyn.coursetable.entity.Semester;
import top.boyn.coursetable.exception.UserException;

import java.util.List;

/**
 * @author Boyn
 * @date 2019/9/22
 */
public interface SemesterService {
    /**
     * 从网络中获取学期与周数的信息,在项目启动时进行检查
     */
    public List<Semester> getSemesterFromNet(String userKey) throws UserException;

    /**
     * 从数据库中获取学期与周数
     */
    public List<Semester> getSemesterWeekFromDB();

    /**
     * 按照学号获取对应的学年
     */
    public List<Semester> getSemesterWeek(int year);

}
