package top.boyn.coursetable;

import cn.hutool.core.io.resource.ClassPathResource;
import cn.hutool.core.io.resource.Resource;
import cn.hutool.crypto.asymmetric.RSA;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.CrossOrigin;
import top.boyn.coursetable.constant.SEMESTER_MAP;
import top.boyn.coursetable.entity.Semester;
import top.boyn.coursetable.service.SemesterService;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@SpringBootApplication
@MapperScan(basePackages = "top.boyn.coursetable.repository")
@Slf4j
public class CoursetableApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoursetableApplication.class, args);
    }

    /**
     * 配置FastJSON
     */
    @Bean
    public HttpMessageConverters fastJsonHttpMessageConverters() {
        //1、定义一个convert转换消息的对象
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
        //2、添加fastjson的配置信息
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat);
        //3、在convert中添加配置信息
        fastConverter.setFastJsonConfig(fastJsonConfig);
        //4、将convert添加到converters中
        HttpMessageConverter<?> converter = fastConverter;
        return new HttpMessageConverters(converter);
    }

    @Bean
    public RSA getRsaFromFileWhenLoad(){
        log.info("Load rsa from file");
        Resource resource = new ClassPathResource("classpath:rsa/pri");
        String privateKey = resource.readUtf8Str();
        resource = new ClassPathResource("classpath:rsa/pub");
        String publicKey = resource.readUtf8Str();
        return new RSA(privateKey, publicKey);
    }

    @Bean
    @Order(43)
    @Autowired
    public SEMESTER_MAP loadSemester(SemesterService semesterService){
        List<Semester> list = semesterService.getSemesterWeekFromDB();
        log.info("Load semester, size is {}", list.size());
        SEMESTER_MAP s = new SEMESTER_MAP();
        SEMESTER_MAP.semesterYearPartMap = list.stream()
                .collect(Collectors.groupingBy(Semester::getSemesterYear));
        return s;
    }
}
