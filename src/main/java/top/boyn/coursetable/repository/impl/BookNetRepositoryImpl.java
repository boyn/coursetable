package top.boyn.coursetable.repository.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import top.boyn.coursetable.entity.Book;
import top.boyn.coursetable.exception.EXCEPTION_CODE;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.repository.BookNetRepository;

import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Boyn
 * @date 2019/9/23
 */
@Slf4j
@Repository
@Primary
public class BookNetRepositoryImpl implements BookNetRepository {
    /* 获取的书本的数量 */
    private static final int BOOK_NUMBER = 6;

    @Override
    public Book getBookInfoFromNet(String subject) throws UserException {
        log.info("请求书目:{}", subject);
        if (StringUtils.isEmpty(subject)) {
            throw new UserException(EXCEPTION_CODE.CHECK_ERROR);
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy");
        Date date = new Date();
        /* 当前年份 */
        int currentYear = Integer.parseInt(simpleDateFormat.format(date));

        Connection connection = Jsoup.connect("http://210.45.242.5:8080/opac/ajax_search_adv.php");

        /* 设置请求头 */
        connection.header("Content-Type", "application/json");

        /* 设置携带的请求数据 */
        connection.requestBody("{\"searchWords\":[{\"fieldList\":[{\"fieldCode\":\"\",\"fieldValue\":\"" +
                subject +
                "\",\"$$hashKey\":\"5N5\"}]}],\"filters\":[],\"unionFilters\":[],\"limiter\":[\"DT:" +
                (currentYear - 1) + "/" + currentYear + "\"]," +
                "\"sortField\":\"relevance\",\"sortType\":\"desc\",\"locale\":\"\",\"pageSize\":20,\"pageCount\":1,\"first\":true}");

        Connection.Response response = null;
        try {
            response = connection.method(Connection.Method.POST).execute();
        } catch (ConnectException e) {
            throw new UserException(EXCEPTION_CODE.NETWORK_REQUEST_ERROR);
        } catch (Exception e) {
            throw new UserException(EXCEPTION_CODE.BOOK_NOT_FOUND_ERROR);
        }

        if (response == null) {
            throw new UserException(EXCEPTION_CODE.BOOK_NOT_FOUND_ERROR);
        }

        /* 返回的书本信息 */
        JSONArray bookInfo = new JSONArray();
        /* 解析返回的json字符串 */
        JSONObject bookPageJSONObject = JSON.parseObject(response.body());
        JSONArray bookPageJSONContentArray = bookPageJSONObject.getJSONArray("content");
        for (int i = 0; i < Math.min(bookPageJSONContentArray.size(), BOOK_NUMBER); i++) {
            JSONObject bookObject = (JSONObject) bookPageJSONContentArray.get(i);
            String url = "http://210.45.242.5:8080/opac/item.php?marc_no=" + bookObject.get("marcRecNo").toString();
//            log.info(url);
            bookInfo.add(getBorrowBooksInfoFromNet(url));
        }
        JSONObject result = new JSONObject();
        result.put("courseName", subject);
        result.put("bookList", bookInfo);
        return JSON.parseObject(result.toJSONString(), Book.class);
    }

    /* 通过书本的url获取书的具体信息 */
    private JSONObject getBorrowBooksInfoFromNet(String url) throws UserException {

        if (StringUtils.isEmpty(url)) {
            throw new UserException(EXCEPTION_CODE.BOOK_NAME_BLANK_ERROR);
        }

        Connection connection = Jsoup.connect(url);

        Connection.Response response = null;
        try {
            response = connection.method(Connection.Method.POST).execute();
        } catch (ConnectException e) {
            throw new UserException(EXCEPTION_CODE.NETWORK_REQUEST_ERROR);
        } catch (Exception e) {
            throw new UserException(EXCEPTION_CODE.BOOK_NOT_FOUND_ERROR);
        }

        if (response == null) {
            throw new UserException(EXCEPTION_CODE.BOOK_NOT_FOUND_ERROR);
        }

        JSONObject bookObject = new JSONObject();

        Document document = Jsoup.parse(response.body());

        /* 书的isbn，用于获取书封面的url */
        String isbn = "";
        /* 书名 */
        String bookName = "";
        /* 作者 */
        String bookAuthor = "";
        /* 借阅书籍信息数组 */
        JSONArray borrowBooksArray = new JSONArray();

        Elements elements = document.select("#item_detail > dl");
        for (Element element : elements) {
            String infoName = element.select("dt").text();
            String info = element.select("dd").text();

            if (infoName.equals("ISBN及定价:")) {
                isbn = info.substring(0, 17);
            }

            if (infoName.equals("题名/责任者:")) {
                bookName = element.select("dd > a").text();
                bookAuthor = element.select("dd").text().split("/")[1];
            }

        }

        /* 书的封面url */
        bookObject.put("bookUrl", "https://www.yuntu.io/book/cover?isbn=" + isbn);
        bookObject.put("bookName", bookName);
        bookObject.put("bookAuthor", bookAuthor);
        bookObject.put("borrowBooks", borrowBooksArray);

        Elements borrowElements = document.select(".whitetext");
        for (Element borrowElement : borrowElements) {
            JSONObject borrowBook = new JSONObject();
            /* 索书号 */
            borrowBook.put("callNum", borrowElement.select("td:nth-child(1)").text());
            /* 条码号 */
            borrowBook.put("barCodeNum", borrowElement.select("td:nth-child(2)").text());
            /* 年卷期 */
            borrowBook.put("annVolume", borrowElement.select("td:nth-child(3)").text());
            /* 馆藏地 */
            String collectPlace = borrowElement.select("td:nth-child(4)").text();
            //在此处删除的不只是空格,还有一个16进制编码为C2A0的占位符(全角空格),所以正则中有两个匹配,空格与占位符
            borrowBook.put("collectPlace", RegExUtils.removeAll(collectPlace, "[  ]"));
            /* 书刊状态 */
            borrowBook.put("bookStatus", borrowElement.select("td:nth-child(5) > font").text());
            borrowBooksArray.add(borrowBook);
        }

        return bookObject;
    }
}
