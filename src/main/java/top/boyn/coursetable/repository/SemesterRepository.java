package top.boyn.coursetable.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.boyn.coursetable.entity.Semester;

/**
 * @author Boyn
 * @date 2019/9/19
 * 学期库类,存放在Mysql中
 */
public interface SemesterRepository extends BaseMapper<Semester> {
}
