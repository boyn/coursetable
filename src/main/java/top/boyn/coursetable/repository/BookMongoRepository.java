package top.boyn.coursetable.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import top.boyn.coursetable.entity.Book;

/**
 * @author Boyn
 * @date 2019/9/19
 * 书本的数据库驱动,存放在Mongodb中
 */
public interface BookMongoRepository extends MongoRepository<Book,String> {
}
