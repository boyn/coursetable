package top.boyn.coursetable.repository;

import top.boyn.coursetable.entity.Book;
import top.boyn.coursetable.exception.UserException;

/**
 * @author Boyn
 * @date 2019/9/23
 */
public interface BookNetRepository {
    Book getBookInfoFromNet(String subject) throws UserException;
}
