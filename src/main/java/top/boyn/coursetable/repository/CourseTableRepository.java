package top.boyn.coursetable.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import top.boyn.coursetable.entity.StudentCourse;

/**
 * @author Boyn
 * @date 2019/9/21
 * 课程表的数据库驱动,存放在Mongodb中
 */
public interface CourseTableRepository extends MongoRepository<StudentCourse, String> {
}
