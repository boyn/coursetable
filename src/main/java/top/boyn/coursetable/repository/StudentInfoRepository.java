package top.boyn.coursetable.repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import top.boyn.coursetable.entity.StudentInfo;

/**
 * @author Boyn
 * @date 2019/9/19
 * 学生信息类,存放在Mysql中
 */
public interface StudentInfoRepository extends BaseMapper<StudentInfo> {
}
