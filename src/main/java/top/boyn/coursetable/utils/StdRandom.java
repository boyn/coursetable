package top.boyn.coursetable.utils;


import java.util.Date;
import java.util.Random;

/**
 * @author Boyn
 * @date 2019/9/4
 * @description 生成随机数类
 */
public class StdRandom {

    /**
     * 生成随机数,范围是[a,b)
     */
    public static int randomInt(int a, int b) {
        return (new Random().nextInt(b - a))+a;
    }

    /**
     * 生成随机数,范围是[0,b)
     */
    public static int randomInt(int b) {
        return (new Random().nextInt(b));
    }

    /**
     * return a date in not long before and not far future
     * 系数在0.0005左右是大约前后半个月
     */
    public static Date randomDate(){
        Date date = new Date();
        long nowTime = date.getTime();
        long beforeTime = nowTime - (long)(nowTime*0.0005);
        long futureTime = nowTime + (long)(nowTime*0.0005);
        long newTime = beforeTime + (long)((futureTime-beforeTime)*new Random().nextDouble());
        return new Date(newTime);
    }
}
