package top.boyn.coursetable.utils;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.Getter;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Boyn
 * @date 2019/9/17
 * @description 统一请求构建类
 */
@Data
public class Result<T> {
    private int statusCode;
    private String message;
    private boolean isSuccess;
    private T data;

    private Result(int statusCode, String message,boolean isSuccess) {
        this.isSuccess = isSuccess;
        this.statusCode = statusCode;
        this.message = message;
    }

    private Result(int statusCode, String message,boolean isSuccess, T data) {
        this.statusCode = statusCode;
        this.isSuccess = isSuccess;
        this.message = message;
        this.data = data;
    }

    public static <T> Result <T> OK() {
        return new Result<T>(200, "success",true);
    }

    public static <T> Result <T> success(T value) {
        return new Result<T>(200, "success", true, value);
    }

    public static <T> Result <T> UserError() {
        return new Result<T>(400, "UserError",false);
    }

    public static <T> Result <T> ServerError() {
        return new Result<T>(500, "ServerError",false);
    }

    public static Result CodeAndMessage(@NotNull int code, @NotNull String message,@NotNull boolean isSuccess) {
        return new Result(code, message,isSuccess);
    }

    public Result<T> code(@NotNull int code) {
        this.statusCode = code;
        return this;
    }

    public Result<T> message(@NotNull String message) {
        this.message = message;
        return this;
    }

    public Result<T> appendInfo(@NotNull T value) {
        this.data = value;
        return this;
    }

}
