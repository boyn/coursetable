package top.boyn.coursetable.utils;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import top.boyn.coursetable.exception.EXCEPTION_CODE;
import top.boyn.coursetable.exception.UserException;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;

/**
 * @author Boyn
 * @date 2019/9/16
 * @description 统一请求库
 */
@Slf4j
public class Request {
    private static final MediaType MediaType_JSON = MediaType.get("application/json; charset=utf-8");
    private static OkHttpClient client = new OkHttpClient();

    /**
     * 同步Get方法,传入URL,参数
     */
    public static Map<String, Object> Get(String url) throws UserException {
        return Get(url, null, null);
    }

    /**
     * 同步Get方法,传入URL,参数和头部
     */
    public static Map<String, Object> Get(String url, Map<String, String> parameters, Map<String, String> headers) throws UserException {
        if (parameters != null && !parameters.isEmpty()) {
            StringBuilder builder = new StringBuilder(url);
            builder.append("?");
            boolean isFirst = true;
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                if (isFirst) {
                    isFirst = false;
                } else {
                    builder.append("&");
                }
                builder.append(entry.getKey());
                builder.append("=");
                builder.append(entry.getValue());
            }
            url = builder.toString();
        }
        okhttp3.Request request;
        if (headers != null && !headers.isEmpty()) {
            okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder()
                    .url(url);
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                requestBuilder.header(entry.getKey(), entry.getValue());
            }
            request = requestBuilder.build();
        } else {
            request = new okhttp3.Request.Builder()
                    .url(url)
                    .build();
        }


        try (Response response = client.newCall(request).execute()) {
            return JSON.parseObject((Objects.requireNonNull(response.body())).string());
        } catch (IOException e) {
            throw new UserException(EXCEPTION_CODE.NETWORK_REQUEST_ERROR);
        }
    }

    /**
     * 同步Post方法,表单传输,传入URL,参数
     */
    public static Map<String, Object> PostByFormData(String url, Map<String, String> parameters) throws UserException {
        return PostByFormData(url, parameters, null);
    }


    /**
     * 同步Post方法,表单传输,传入URL,参数和头部
     */
    public static Map<String, Object> PostByFormData(String url, Map<String, String> parameters, Map<String, String> headers) throws UserException {
        FormBody body;
        if (parameters != null && !parameters.isEmpty()) {
            FormBody.Builder bodyBuilder = new FormBody.Builder();
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                bodyBuilder.add(entry.getKey(), entry.getValue());
            }
            body = bodyBuilder.build();
        } else {
            return null;//若无参数,则post请求返回错误
        }
        okhttp3.Request request;
        if (headers != null && !headers.isEmpty()) {
            okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder()
                    .url(url).post(body);
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                requestBuilder.header(entry.getKey(), entry.getValue());
            }
            request = requestBuilder.build();
        } else {
            request = new okhttp3.Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
        }

        try (Response response = client.newCall(request).execute()) {
            return JSON.parseObject((Objects.requireNonNull(response.body())).string());
        } catch (IOException e) {
            throw new UserException(EXCEPTION_CODE.NETWORK_REQUEST_ERROR);
        }

    }

    /**
     * 同步Post方法,JSON传输,传入URL,参数
     */
    public static Map<String, Object> PostByJSONData(String url, Map<String, String> parameters) throws UserException {
        return PostByJSONData(url, parameters, null); 
    }

    /**
     * 同步Post方法,JSON传输,传入URL,参数和头部
     */
    public static Map<String, Object> PostByJSONData(String url, Map<String, String> parameters, Map<String, String> headers) throws UserException {
        String parametersString = JSON.parseObject(parameters.toString()).toJSONString();
        RequestBody body = RequestBody.create(parametersString, MediaType_JSON);
        okhttp3.Request request;
        if (headers != null && !headers.isEmpty()) {
            okhttp3.Request.Builder requestBuilder = new okhttp3.Request.Builder()
                    .url(url).post(body);
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                requestBuilder.header(entry.getKey(), entry.getValue());
            }
            request = requestBuilder.build();
        } else {
            request = new okhttp3.Request.Builder()
                    .url(url)
                    .url(url)
                    .post(body)
                    .build();
        }
        try (Response response = client.newCall(request).execute()) {
            return JSON.parseObject((Objects.requireNonNull(response.body())).string());
        } catch (IOException e) {
            throw new UserException(EXCEPTION_CODE.NETWORK_REQUEST_ERROR);
        }
    }
}
