package top.boyn.coursetable.utils;

import lombok.extern.slf4j.Slf4j;
import top.boyn.coursetable.constant.SEMESTER_MAP;
import top.boyn.coursetable.entity.Semester;
import top.boyn.coursetable.exception.EXCEPTION_CODE;
import top.boyn.coursetable.exception.UserException;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * @author Boyn
 * @date 2019/9/22
 */
@Slf4j
public class Validator {
    /**
     * 检查学期和周数是否合法
     * 从SEMESTER常量类中获取学期和周数的长度进行验证
     */
    public static boolean checkSemesterAndWeekIsValid(String semester,String part, String week) throws UserException {
        Map<Integer, List<Semester>> map = SEMESTER_MAP.semesterYearPartMap;
        int semesterCode,partCode,weekCode;
        try {
            partCode = Integer.parseInt(part);
            semesterCode = Integer.parseInt(semester);
            weekCode = Integer.parseInt(week);
        } catch (NumberFormatException e) {
            throw new UserException(EXCEPTION_CODE.CHECK_ERROR);
        }

        if (!map.containsKey(semesterCode)) {
            throw new UserException(EXCEPTION_CODE.SEMESTER_YEAR_ERROR);
        }
        if (partCode != 1 && partCode != 2) {
            throw new UserException(EXCEPTION_CODE.SEMESTER_PART_ERROR);
        }
        int weekLength;
        try {
            weekLength = map.get(semesterCode).stream().filter(o -> o.getSemesterPart() == partCode).mapToInt(Semester::getWeeks).findFirst().getAsInt();
        } catch (NoSuchElementException e) {
            throw new UserException(EXCEPTION_CODE.SEMESTER_PART_ERROR);
        }
        if (weekCode < 1 || weekCode > weekLength) {
            throw new UserException(EXCEPTION_CODE.WEEK_ERROR);
        }
        return true;
    }
}
