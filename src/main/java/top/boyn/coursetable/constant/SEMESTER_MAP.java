package top.boyn.coursetable.constant;

import top.boyn.coursetable.entity.Semester;
import top.boyn.coursetable.exception.EXCEPTION_CODE;
import top.boyn.coursetable.exception.UserException;

import java.util.List;
import java.util.Map;

/**
 * @author Boyn
 * @date 2019/9/10
 * @description 学期对应的表, 作为常量
 * 这个类会在Application中的loadSemester方法初始化时,从数据库中获取
 */
public class SEMESTER_MAP {

    //两层嵌套的Map,第一层参数是年份
    public static Map<Integer, List<Semester>> semesterYearPartMap;

    public static String getSemesterCode(Integer year, Integer part) throws UserException {
        if (semesterYearPartMap == null || semesterYearPartMap.isEmpty()) {
            throw new UserException(EXCEPTION_CODE.SEMESTER_MAP_NOT_INIT);
        }
        List<Semester> semesterList = semesterYearPartMap.get(year);
        if (semesterList == null || semesterList.isEmpty()) {
            throw new UserException(EXCEPTION_CODE.SEMESTER_YEAR_ERROR);
        }
        String code = null;
        for (Semester semester : semesterList) {
            if (part.equals(semester.getSemesterPart())) {
                code = semester.getCode();
            }
        }
        if (code == null) {
            throw new UserException(EXCEPTION_CODE.SEMESTER_PART_ERROR);
        }
        return code;
    }

    public static String getSemesterCode(String year, String part) throws UserException {
        int yearCode = -1;
        int partCode = -1;
        try {
            yearCode = Integer.parseInt(year);
        } catch (Exception e) {
            throw new UserException(EXCEPTION_CODE.SEMESTER_YEAR_ERROR);
        }
        try {
            partCode = Integer.parseInt(part);
        } catch (Exception e) {
            throw new UserException(EXCEPTION_CODE.SEMESTER_PART_ERROR);
        }
        return getSemesterCode(yearCode, partCode);
    }

}
