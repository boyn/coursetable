package top.boyn.coursetable.secure;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import top.boyn.coursetable.exception.EXCEPTION_CODE;
import top.boyn.coursetable.exception.UserException;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/21
 * 用来验证凭证是否正确
 */
@Component
@Slf4j
public class TokenTool {
    @Resource
    private RSA rsa;

    public boolean isValid(String key) throws Exception{
        key = key.replaceAll(" ", "+");
        try {
            String json = StrUtil.str(rsa.decrypt(Base64.decode(key), KeyType.PrivateKey), CharsetUtil.CHARSET_UTF_8);
            log.info("Authorization Information: "+json);
        } catch (Exception e) {
            throw new UserException(EXCEPTION_CODE.AUTHORIZATION_WRONG);
        }
        return true;
    }

    public JSONObject getInfo(String key) throws Exception{
        key = key.replaceAll(" ", "+");
        String json = StrUtil.str(rsa.decrypt(Base64.decode(key), KeyType.PrivateKey), CharsetUtil.CHARSET_UTF_8);
        log.info("Get Info in Authorization: "+json);
        return JSON.parseObject(json);
    }

    public int getYear(String key) throws Exception {
        key = key.replaceAll(" ", "+");
        String json = StrUtil.str(rsa.decrypt(Base64.decode(key), KeyType.PrivateKey), CharsetUtil.CHARSET_UTF_8);
        return Integer.parseInt(JSON.parseObject(json).getString("number").substring(0, 4));
    }
}
