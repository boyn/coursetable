package top.boyn.coursetable.entity;

import lombok.Data;

import java.util.List;

/**
 * @author Boyn
 * @date 2019/9/19
 * @description 学生课程
 */
@Data
public class StudentCourse {
    //StudentInfo存放在数据库中的ID
    private Long StudentID;
    //学年
    private Integer semesterYear;
    //学年中的学期
    private Integer semesterPart;
    //周数
    private Integer week;
    //课程
    private List<Course> courseList;
}
