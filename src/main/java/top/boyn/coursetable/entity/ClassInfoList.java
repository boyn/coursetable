package top.boyn.coursetable.entity;

import lombok.Data;

import java.util.List;

/**
 * @author Boyn
 * @date 2019/9/23
 */
@Data
public class ClassInfoList {

    private String lessonCode;

    private List<StudentInClassInfo> students;

    private Integer listLength;

    @Data
    public static class StudentInClassInfo {
        //姓名
        private String name;
        //性别
        private String gender;
        //班级
        private String className;
    }
}
