package top.boyn.coursetable.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.List;

/**
 * @author Boyn
 * @date 2019/9/17
 * @description Book的实体类
 */
@Data
public class Book {
    /**
     * Mongodb的_id字段
     */
    @Id
    @JSONField(serialize = false)
    private String _id;

    /**
     * 以courseName作为索引
     */
    @Indexed
    //课程名
    private String courseName;
    //书列表
    private List<BookInfo> bookList;


    @Data
    private static class BookInfo {
        //书本图片的网络地址
        private String bookUrl;
        //书本名称
        private String bookName;
        //书本作者
        private String bookAuthor;
        //可借的书信息
        private List<BorrowBook> borrowBooks;
    }

    @Data
    private static class BorrowBook {
        //书本是否可借
        private String bookStatus;
        //年份
        private String annVolume;
        //索书号
        private String callNum;
        //条码
        private String barCodeNum;
        //馆藏地点
        private String collectPlace;
    }
}
