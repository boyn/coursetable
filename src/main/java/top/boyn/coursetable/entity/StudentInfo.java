package top.boyn.coursetable.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author Boyn
 * @date 2019/9/10
 * @description 学生信息
 */
@Data
@TableName("student_info")
public class StudentInfo {
    @TableId("id")
    private Long ID;//数据库中的ID
    @TableField("number")
    private String number;//学号
    @TableField("name")
    private String name;//姓名
    @TableField("password")
    private String password;//密码
    @TableField("user_key")
    private String userKey;//安卓APP请求所用的userKey
}
