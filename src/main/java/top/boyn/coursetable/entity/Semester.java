package top.boyn.coursetable.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author Boyn
 * @date 2019/9/19
 * @description 学期实体类
 */
@TableName("semester")
@Data
public class Semester {
    @TableField("code")
    //学期代码
    private String code;

    @TableField("name")
    //学期的名称
    private String name;


    @TableField("weeks")
    //学期的总周数
    private Integer weeks;

    @TableField("start_date")
    //学期开始的日期
    private String startDate;

    @TableField("semester_year")
    //当前学年
    private Integer semesterYear;

    @TableField("semester_part")
    //学年的第一部分还是第二部分,用1,2表示
    private Integer semesterPart;
}
