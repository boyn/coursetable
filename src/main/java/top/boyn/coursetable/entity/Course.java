package top.boyn.coursetable.entity;

import lombok.Data;

/**
 * @author Boyn
 * @date 2019/9/19
 * @description 课程实体类
 */
@Data
public class Course {
    //课程名
    private String courseName;
    //教师
    private String[] teachers;
    //课程代码
    private String lesson_code;
    //上课人数
    private int number;
    //在本周的哪一天
    private Integer weekDay;
    //开始节数
    private Integer startUnit;
    //结束节数
    private Integer endUnit;
    //需要上课的节数
    private String activateWeek;
    //校区
    private String campus;
    //教室名
    private String classRoomName;
}
