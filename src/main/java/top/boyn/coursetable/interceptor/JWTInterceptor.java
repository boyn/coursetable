package top.boyn.coursetable.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.WebRequestInterceptor;
import top.boyn.coursetable.exception.EXCEPTION_CODE;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.secure.TokenTool;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/21
 */
@Slf4j
public class JWTInterceptor implements WebRequestInterceptor {
    @Resource
    TokenTool authorization;

    @Override
    public void preHandle(WebRequest request) throws Exception {
        String message = request.getHeader("Authorization");
        if (message == null) {
            throw new UserException(EXCEPTION_CODE.NO_LOGIN);
        }else{
            if (!authorization.isValid(message)) {
                throw new UserException(EXCEPTION_CODE.AUTHORIZATION_NO_PASS);
            }
            String number = authorization.getInfo(message).getString("number");
            Integer year = authorization.getYear(message);
            log.info("Inject number: {}, year: {}",number,year);
            request.setAttribute("number", number, 0);
            request.setAttribute("year",year,0);
        }
    }

    @Override
    public void postHandle(WebRequest request, ModelMap model) throws Exception {

    }

    @Override
    public void afterCompletion(WebRequest request, Exception ex) throws Exception {

    }
}
