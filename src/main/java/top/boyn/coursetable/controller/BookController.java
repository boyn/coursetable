package top.boyn.coursetable.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.service.BookService;
import top.boyn.coursetable.utils.Result;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/17
 * @description
 */
@RestController
@RequestMapping("/api/book")
public class BookController {
    @Resource
    BookService service;

    @GetMapping("/{bookName}")
    public Object getBookByName(@PathVariable String bookName) throws UserException {
        return Result.OK().appendInfo(service.getBookInfo(bookName));
    }
}
