package top.boyn.coursetable.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import top.boyn.coursetable.secure.TokenTool;
import top.boyn.coursetable.service.SemesterService;
import top.boyn.coursetable.utils.Result;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/22
 */
@RestController
@RequestMapping("/api/semester")
public class SemesterController {
    @Resource
    SemesterService semesterService;

    @GetMapping("")
    public Object getSemester(WebRequest request) throws Exception {
        Integer year = (Integer) request.getAttribute("year", 0);
        return Result.OK().appendInfo(semesterService.getSemesterWeek(year));
    }
}
