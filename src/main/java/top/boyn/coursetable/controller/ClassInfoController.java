package top.boyn.coursetable.controller;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import top.boyn.coursetable.entity.ClassInfoList;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.service.ClassInfoListService;
import top.boyn.coursetable.service.StudentInfoService;
import top.boyn.coursetable.utils.Result;

import javax.annotation.Resource;

/**
 * @author Boyn
 * @date 2019/9/23
 */
@RequestMapping("/api/classInfo")
@RestController
public class ClassInfoController {
    @Resource
    ClassInfoListService listService;
    @Resource
    StudentInfoService infoService;
    @GetMapping("/{year}/{part}/{courseCode}")
    public Object getClassInfoList(@PathVariable String year,
                                   @PathVariable String part,
                                   @PathVariable String courseCode,
                                   WebRequest request) throws UserException {
        String number = (String) request.getAttribute("number", 0);
        String userKey = infoService.getUserKeyByNumber(number);
        ClassInfoList classInfoList = listService.getClassInfoList(year, part, courseCode, userKey);
        return Result.OK().appendInfo(classInfoList);
    }
}
