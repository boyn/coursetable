package top.boyn.coursetable.controller;

import cn.hutool.crypto.asymmetric.RSA;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import top.boyn.coursetable.exception.UserException;
import top.boyn.coursetable.service.LoginService;
import top.boyn.coursetable.utils.Result;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author Boyn
 * @date 2019/9/10
 * 客户端获取公钥,并用公钥进行加密,将加密后的文本发到服务端中
 * 服务端使用私钥进行解密
 * 发送过来的数据应该为JSON字符串加密
 * JSON字符串格式如下
 * {
 *     username: //学号
 *     password: //密码
 * }
 */
@RestController
@RequestMapping("/login")
@Slf4j
public class LoginController {
    @Resource
    LoginService service;

    @Resource
    RSA rsa;

    @GetMapping("/publicKey")
    public Object publicKey(HttpServletRequest request) {
        log.info("Get PublicKey");
        String publicKey = rsa.getPublicKeyBase64();
        return Result.OK().appendInfo(publicKey);
    }

    @PostMapping("/loginKey")
    public Object login(@ModelAttribute("key") @NotNull @NotEmpty String key) throws UserException{
        String info = service.parseKey(key);
        log.info("Log in. UserInfo: {}", info);
        return Result.OK().appendInfo(service.login(info));
    }
}
