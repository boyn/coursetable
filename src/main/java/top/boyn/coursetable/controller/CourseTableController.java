package top.boyn.coursetable.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import top.boyn.coursetable.entity.Course;
import top.boyn.coursetable.secure.TokenTool;
import top.boyn.coursetable.service.CourseTableService;
import top.boyn.coursetable.utils.Result;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Boyn
 * @date 2019/9/17
 * @description
 */
@RestController
@Slf4j
@RequestMapping("/api/coursetable")
public class CourseTableController {
    @Resource
    CourseTableService service;

    @GetMapping("/{semesterCode}/{part}/{weekCode}")
    public Object getCourseTable(@PathVariable String semesterCode, @PathVariable String part, @PathVariable String weekCode, WebRequest request) throws Exception {
        String number = (String) request.getAttribute("number", 0);
        List<Course> courses = service.getCourseTable(semesterCode, part, weekCode, number);
        return Result.OK().appendInfo(courses);
    }
}
