package top.boyn.coursetable.exception;

/**
 * @author Boyn
 * @date 2019/9/21
 */
public class UserException extends Exception {
    public UserException() {
        super();
    }

    public UserException(EXCEPTION_CODE code) {
        this.code = code.getCode();
        this.message = code.getMessage();
    }

    public int code;
    public String message;
}
