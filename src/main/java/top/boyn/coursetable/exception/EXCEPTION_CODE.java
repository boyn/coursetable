package top.boyn.coursetable.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Boyn
 * @date 2019/9/21
 */
@AllArgsConstructor
@Getter
public enum EXCEPTION_CODE {

    //1000开头为参数的错误
    USERNAME_OR_PASSWORD_ERROR(1001, "用户名或密码错误"),
    CHECK_ERROR(1002, "参数检查不通过"),
    BOOK_NAME_BLANK_ERROR(1003, "书名为空"),
    SEMESTER_YEAR_ERROR(1004, "学年选择错误"),
    SEMESTER_PART_ERROR(1005, "学期选择错误"),
    WEEK_ERROR(1006, "周数选择错误"),
    ONLY_BACHELOR_ERROR(1007,"仅支持本科生"),
    JSON_INVALID_ERROR(1008,"JSON格式不合法"),
    COURSE_CODE_ERROR(1009,"课程代码输入错误"),

    //2000开头为数据库中信息的错误
    NO_THIS_STUDENT_ERROR(2001,"无此学生"),
    COURSE_TABLE_NOT_FOUND_ERROR(2002,"找不到课程表"),
    BOOK_NOT_FOUND_ERROR(2003,"找不到该书"),

    //3000开头为验证时的错误
    AUTHORIZATION_NO_PASS(3001, "验证不通过"),
    NO_LOGIN(3002, "未登录"),
    AUTHORIZATION_WRONG(3003, "验证信息错误"),
    KEY_IS_BLANK(3004, "传入key为空"),
    KEY_IS_NULL(3005, "传入key为null"),
    KEY_PARSE_ERROR(3006, "传入key解析出错"),

    //4000开头为程序错误
    SEMESTER_MAP_NOT_INIT(4001, "Semester常量未被初始化"),
    NETWORK_REQUEST_ERROR(4002, "网络请求出错"),

    //暂时保留这些错误
    UNKNOWN_ERROR(5000,"未知错误,请检查请求"),

    NO_INFORMATION(4003, "信息不存在"),
    PARAMETER_ERROR(4004,"参数有问题");

    private int code;
    private String message;


}
